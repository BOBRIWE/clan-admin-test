import IDestinyHistoricalStatsPeriodGroup from './IDestinyHistoricalStatsPeriodGroup';

export default interface IDestinyActivityHistoryResults {
    activities: IDestinyHistoricalStatsPeriodGroup[]
}