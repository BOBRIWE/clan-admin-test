FROM node:12 AS build
WORKDIR /app
COPY . /app
RUN yarn install
RUN yarn run build


FROM nginx
COPY --from=build /app/build/ /usr/share/nginx/html
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
ENV PORT 5000
EXPOSE 5000
EXPOSE 80


#FROM node:12 AS build
#
#WORKDIR /temp
#COPY . /temp
#
#
#
#RUN yarn install
#RUN yarn run build
#
#WORKDIR /
#COPY --from=build /app /app
#ENV PORT 5000
#EXPOSE 5000